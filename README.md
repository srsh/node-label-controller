# node-label-controller
This is a controller which watches the Kubernetes nodes and sets a label to the Kubernetes Node object when the node uses ContainerLinux as operating system.


If a cluster contains a mix of Nodes (different OS Images), it can cause problems for the ContainerLinuxUpdateOperator, as it should only run on ContainerLinux nodes.
This controller:
1. watches the Kubernetes Node objects;
2. checks if any Node uses ContainerLinux;
3. attaches a label to the Node if it uses ContainerLinux (`kubermatic.io/uses-container-linux: 'true'`)


For that, the [Container Linux Update Operator](https://github.com/coreos/container-linux-update-operator) was deployed using the manifests from its repository, but with set NodeSelector for nodes with label `kubermatic.io/uses-container-linux: "true"`.


It was built with use of [controller-runtime library](https://github.com/kubernetes-sigs/controller-runtime) and with help of explicitly [this example](https://github.com/kubernetes-sigs/controller-runtime/tree/master/examples/builtins). 


## structure
The repo contains: 
- [main.go](main.go): 
    - creates a new manager
    - creates a new controller that watches and reconciles nodes
    - starts the manager
- [controller.go](controller.go): implements a reconciler for nodes
- [deploy/container-linux-update-operator](deploy/container-linux-update-operator): deploys the [Container Linux Update Operator](https://github.com/coreos/container-linux-update-operator)
- [deploy/node-label-controller](deploy/node-label-controller): deploys the `node-label-controller` implemented in `main.go` and `controller.go`
- [Dockerfile](Dockerfile): file with which I have built the `node-label-controller` image


## result
A node with OS Image `Container Linux by CoreOS 2303.3.0 (Rhyolite)` got applied a new label `kubermatic.io/uses-container-linux=true`, wheras other nodes did not. 

## most useful materials & resources
- https://github.com/kubernetes-sigs/controller-runtime
- https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.17/#node-v1-core
- https://book.kubebuilder.io/cronjob-tutorial/controller-implementation.html
- https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/
