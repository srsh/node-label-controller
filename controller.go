// Marta Skarbek-Kazanecka, January 2020

/*
Copyright 2018 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"strings"

	"github.com/go-logr/logr"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// reconcilePod reconciles Pods
type reconcileNode struct {
	// client can be used to retrieve objects from the APIServer.
	client client.Client
	log    logr.Logger
}

const (
	labelKey = "kubermatic.io/uses-container-linux"
	labelValue = "true"
)

// Implement reconcile.Reconciler so the controller can reconcile objects
var _ reconcile.Reconciler = &reconcileNode{}

func (r *reconcileNode) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	// set up a convenient log object so we don't have to type request over and over again
	log := r.log.WithValues("request", request)

	// Fetch the node
	node := &corev1.Node{}
	err := r.client.Get(context.TODO(), request.NamespacedName, node)
	if errors.IsNotFound(err) {
		log.Error(nil, "Could not find Node")
		return reconcile.Result{}, nil
	}

	if err != nil {
		log.Error(err, "Could not fetch Node")
		return reconcile.Result{}, err
	}

	// Print the information about which node is reconciled
	log.Info("Reconciling Node", node.Name)

	// Check OS of the node
	if ( strings.Contains(node.Status.NodeInfo.OSImage, "ContainerLinux") || strings.Contains(node.Status.NodeInfo.OSImage, "Container Linux")) {
		// Set the labels if not present
		if node.Labels == nil {
			node.Labels = map[string]string{}
		}

		// Check if the needed label is set and if not, set it & update the node
		if node.Labels[labelKey] != labelValue {
			node.Labels[labelKey] = labelValue
			// Update the Node
			err = r.client.Update(context.TODO(), node)
			if err != nil {
				log.Error(err, "Could not write Node")
				return reconcile.Result{}, err
			}
		}	
	}
	return reconcile.Result{}, nil
}
